$(document).ready(function () {
    $('.date-period').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_4",
        format: 'DD/MM/YYYY',
        locale: { 
            daysOfWeek: [ "Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab" ],
            monthNames: [ "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre" ],
            firstDay: 1,
        }
    }, function (start, end, label) {
        //console.log(start.toISOString(), end.toISOString(), label);
    });  

    $('#period-reset').click(function() {
        var d = new Date();
        var curr_date 	= d.getDate();
        var curr_month 	= d.getMonth()+1;
        var curr_year 	= d.getFullYear();

        if (curr_month < 10) {
            curr_month = "0"+curr_month;
        }
        
        var begin 	= "01/"+curr_month+"/"+curr_year;
        var end 	= curr_date+"/"+curr_month+"/"+curr_year;

        $('#date-period-begin').val(begin);
        $('#date-period-end').val(end);
    });		

    /* Attivo il Datepicker per le date */
    $('#payed_at').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_4",
        format: 'DD/MM/YYYY',
        locale: { 
            daysOfWeek: [ "Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab" ],
            monthNames: [ "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre" ],
            firstDay: 1,
        }
    }, function (start, end, label) {
        //console.log(start.toISOString(), end.toISOString(), label);
    });
    
});
