<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $fillable = [
		'name'
	];

	/**
     * Imposto l'associazione 1:N con le spese.
     */
	public function expenses() {
		return $this->hasMany('App\Expense');
	}
}
