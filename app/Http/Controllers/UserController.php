<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
 	public function __construct() 
 	{
		$this->middleware('auth');	
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$users = User::all();
				
		return view('user.index', compact('users'));
    }
    
	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() 
	{
		return view('user.create');
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request)
    {        
		$validatedData = $request->validate([
			'name' 					=> 'required|min:3',
			'surname' 				=> 'required|min:3',
			'email' 				=> 'required|unique:users|email',
			'password' 				=> 'required|confirmed|min:8',
			'password_confirmation' => 'required',
		]);

		$input = $request->all();
		$newUser = User::create($input);

		return redirect('user'); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function edit($id) 
	{
		$user = User::find($id);

		return view('user.edit', compact('user'));
	}
	
	/**
     * Update the specified resource in storage.
     *
     * @param  int  $id
	 * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function update($id, Request $request) 
	{
		$validatedData = $request->validate([
			'name' 					=> 'required|min:3',
			'surname' 				=> 'required|min:3',
			'color'					=> 'required',
			'email' 				=> 'required|email',
			'password' 				=> 'nullable|confirmed|min:8',
			'password_confirmation' => 'sometimes|required_with:password',
		]);
	
		$input = $request->all();
		
		// Se il campo password non viene configurato, allora non cambio 
		// la password ed elimino i campi vuoti dai dati altrimenti si 
		// sovrascriverebbe la password
		
		if (!empty($input['password'])) {
			$input['password'] = bcrypt($input['password']);
		
		} else {
			unset($input['password']);
		}
		
		$user = User::find($id);				
		$user->update($input);
		
		return redirect('user');		
	}
	
	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function destroy(Request $request, $id)
    {			 	
		try {
			$user = User::find($id);
			$user->delete();
		
		} catch (\Illuminate\Database\QueryException $e) {
			return redirect('user')->withErrors(['L\'utente non può essere cancellato']);
		}
		
		return redirect('user');
    }
}
