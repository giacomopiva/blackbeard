<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Expense;
use App\User;
use App\Category;
use Carbon\Carbon;
use DB;

class ExpenseController extends Controller
{
	public function __construct() 
	{
		$this->middleware('auth');	
	}
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */	
	public function index(Request $request) 
	{
		// Imposto due date di default: Primo e ultimo gg del mese
		$begin 	= new Carbon('first day of this month');
		$end 	= new Carbon('last day of this month');

		// Controllo se sono state passate delle date e le prelevo se sono presenti
		$input = $request->all();

		if (isset($input['date-period-begin'])) {
			$begin = Carbon::createFromFormat('d/m/Y', $input['date-period-begin']);
		}

		if (isset($input['date-period-end'])) {
			$end = Carbon::createFromFormat('d/m/Y', $input['date-period-end']);
		}

		// Estraggo tutte le spese del periodo
		$expenses = Expense::orderBy('payed_at', 'asc')
						->whereBetween('payed_at', [$begin, $end])
						->get();
		
		/*			
			Estraggo le spese per categoria:
			SELECT SUM(amount), name
			FROM expenses, categories 
			WHERE expenses.category_id = categories.id
			GROUP BY categories.name, category_id;
		*/
		$category_expenses = DB::table('expenses')
		                     ->select(DB::raw('SUM(expenses.amount) as total_amount'), 'category_id', 'categories.name')
							 ->join('categories', 'expenses.category_id', '=', 'categories.id')
 							 ->whereBetween('payed_at', [$begin, $end])
		                     ->groupBy('categories.name', 'expenses.category_id')
		                     ->get();
				
		// Estraggo tutti gli utenti per passarli perchè mi serovno per formattare bene i campi
		$users = User::all();

		// Estraggo tutte le categorie
		$categories = Category::all();
		
		// Calcolo le spese totali per utente
		
		// Array che contiene la spesa per ogni utente + altre informazioni
		$user_expenses = $this->userExpenses($users, $expenses);
		
		// Totale delle spese sostenute da tutti gli utenti
		$total_expenses = $this->totalExpenses($expenses);
				
		// Aggiungo un array che indica quanto un utente deve dare agli altri utenti
		foreach ($user_expenses as $index => $user) {	
			$user_expenses[$index]['to_give'] = $this->addToGives($user, $user_expenses);
		}
	
		return view('expense.index', compact('expenses', 'user_expenses', 'total_expenses', 'users', 'begin', 'end', 'category_expenses'));
	}
	
	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function create() 
	{
		$users = User::all();
		$categories = Category::all();

		return view('expense.create', compact('users', 'categories'));
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */	
	public function store(Request $request) 
	{
		$validatedData = $request->validate([
			'payed_at'		=> 'required',
			'description'		=> 'required|min:3',
			'amount'		=> 'required|min:1.00',
			'user_id'		=> 'required',
			'category_id'	=> 'required'
		]);
		
		$input = $request->all();
		Expense::create($input);
		
		return redirect('expense/create')->with('success', 'Nuova spesa aggiunta con successo!');
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function edit(Int $id) 
	{
		$expense 	= Expense::find($id);
		$users 		= User::all();
		$categories = Category::all();

		return view('expense.edit', compact('expense', 'users', 'categories'));
	}
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function update(Request $request, Expense $expense) 
	{
		$validatedData = $request->validate([
			'payed_at'		=> 'required',
			'description'	=> 'required|min:3',
			'amount'		=> 'required|min:1.00',
			'user_id'		=> 'required',
			'category_id'	=> 'required'
		]);

		$input = $request->all();
		$expense->update($input);
				
		return redirect('expense');		
	}
	
	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function destroy($id) 
	{
		$expense = Expense::find($id);
		$expense->delete();
		
		return json_encode(['status' => 'ok', 'message' => 'Spesa eliminata']);
	}	

	/*
	|--------------------------------------------------------------------------
	| Altre funzioni
	|--------------------------------------------------------------------------
	|
	| Qui definisco funzioni che uso dentro le funzioni principali
	|
	*/
	
	/**
     * Questa funzione calcola il totale delle spese sostenute da ogni utente.
     *
     * @param  Array  $users
	 * @param  Array  $expenses
     * @return Array
     */
	private function userExpenses($users, $expenses) 
	{
		$user_expenses = [];
		
		foreach ($users as $user) {
			$user_expense = 0;
		
			foreach ($expenses as $expense) {
				if ($user->id == $expense->user_id) {
					$user_expense 	+= $expense->amount;
				}
			}
			
			$new = [ "name" 	=> $user->name, 
					 "amount" 	=> $user_expense, 
					 "color" 	=> $user->color, 									 
					 "user_id"	=> $user->id ];
										 
			array_push($user_expenses, $new);				                	
		}
		
		return $user_expenses;
	}

	/**
     * Questa funzione calcola il totale delle spese sostenute da tutti gli utenti.
     *
	 * @param  Array  $expenses
     * @return Array
     */
	private function totalExpenses($expenses) 
	{
		$total_expenses = 0;
		foreach ($expenses as $expense) {
			$total_expenses += $expense['amount'];
		}
		
		return $total_expenses;		
	}
	 
	/**
     * Questa funzione calcola e aggiunge all'array delle spese il dare di un utente per pareggiare i calcoli.
     *
	 * @param  User  $user
	 * @param  Array  $expenses
     * @return Array
     */
	private function addToGives($user, $user_expenses) 
	{
		$to_gives = [];

		foreach ($user_expenses as $partner) {
			if ($user['user_id'] == $partner['user_id']) {
				array_push($to_gives, 0);
				continue;
			}			
			
			$exp_user 		= $user['amount'] / count($user_expenses);
			$exp_partner 	= $partner['amount'] / count($user_expenses);
			
			$to_give = ($exp_partner - $exp_user) > 0 ? abs($exp_partner - $exp_user) : 0;
			array_push($to_gives, $to_give);
		}
		
		return $to_gives;
	}

}
