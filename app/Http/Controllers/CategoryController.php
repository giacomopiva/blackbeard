<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Category;
use Carbon\Carbon;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		  $categories = Category::all();

      return json_encode(['status' => 'ok', 'categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = \Validator::make($request->all(), [
        'name' 	=> 'required|unique:categories|min:3',
      ]);
    
      if ($validator->fails()) {
        return response()->json(['errors' => $validator->errors()->all()]);
      }
      
      $input = $request->all();
		  $newCategory = Category::create($input);
      
      return json_encode(['status' => 'ok', 'message' => 'Categoria aggiunta', 'category' => $newCategory]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      // Sposto tutte le spese della categoria nella categoria 1 ("Altro")
		  $expenses = DB::table('expenses')
          ->where('category_id', '=', $id)
          ->update(array('category_id' => 1));
			
		  $category = Category::find($id);
		  $category->delete();
		
      return json_encode(['status' => 'ok', 'message' => 'Categoria cancellata']);
    }
}
