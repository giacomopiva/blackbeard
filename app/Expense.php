<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Expense extends Model
{
	protected $fillable = [
		'payed_at',
		'description',
		'amount',
		'user_id',
		'category_id'		
	];
	
	public function setPayedAtAttribute($date) {
		// Converto la data che viene inserita nel formato gg/mm/aaaa ed interpretata come mm/gg/aaaa
		$this->attributes['payed_at'] = Carbon::createFromFormat('d/m/Y', $date);
	}
	
	/**
     * Imposto l'associazione 1:N con gli utenti.
     */
	public function user() {
		return $this->belongsTo('App\User');
	}

	public function wallet() {
		return $this->user->name;
	}

    /**
     * Imposto l'associazione 1:N con le categorie.
     */
	public function category() {
		return $this->belongsTo('App\Category');
	}
	
	public function categoryName() {
		return $this->category->name;
	}
	
}
