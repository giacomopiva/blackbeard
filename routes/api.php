<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/**
 * Rotte per la gestione delle categorie AJAX
 */
Route::resource('category', 'CategoryController', ['only' => ['index', 'store', 'destroy']]);
//Route::middleware('auth:api')->resource('category', 'CategoryController', ['only' => ['index', 'store', 'destory']]);
