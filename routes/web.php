<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/** 
 * Rotte per l'autenticazione
 */ 
Auth::routes();

// Personalizzo il percorso del logout per avere il logout come GET e non come POST
Route::get('logout', 'Auth\LoginController@logout'); 

/**
 * Rotta della radice e della Home 
 */ 
Route::get('/', 'ExpenseController@index');

// La rotta home ha anche un "nomiglnolo" per essare chiamata attraverso il metodo route('home')
Route::get('/home', 'ExpenseController@index')->name('home');

/** 
 * Rotte per la gestione degli utenti 
 */ 
Route::resource('user', 'UserController', ['except' => ['destroy']]);

// Personalizzo la DESTROY per gestirla con la GET e non con la POST
Route::get('user/{id}/destroy', 'UserController@destroy');

/**
 * Rotte per la gestione delle spese 
 */ 
Route::resource('expense', 'ExpenseController', ['except' => ['show']]);

/** 
 * Rotte per la gestione delle categorie: dal Web ritorno la View.
 * Le altre rotte sono gestite in api.php 
 */ 
Route::middleware('auth')->get('/category', function () {
    return view('category.index');
})->name('category');
