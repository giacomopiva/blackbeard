<?php

use Illuminate\Database\Seeder;

class ExpensesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();
		
    	$now = date('Y-m-d');

    	for ($i=0; $i<40; $i++) {
			DB::table('expenses')->insert([
		        'payed_at' => date('Y-m-d', strtotime('-'.rand(1,90).' days', strtotime($now))),
		        'description' => '',
		        'amount' => rand(10,100),
		        'user_id' => rand(1,3),
				'category_id' => rand(1,3),
				'updated_at' => date('Y-m-d h:i:s'),
				'created_at' => date('Y-m-d h:i:s')		        		        
			]);			
        }

        //Model::reguard();
    }
}
