@extends('app')

@section('content')

<div class="page-title">
	<div class="title_left"></div>
</div>

<div class="clearfix"></div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	
	    <div class="x_panel">
	        <div class="x_title">
	            <h2>Utenti</h2>
	            <ul class="nav navbar-right panel_toolbox">
	                <li><a class="add-link" href="{{ URL::action('UserController@create') }}"><i class="fa fa-plus"></i></a></li>
	                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
	            </ul>
	            
	            <div class="clearfix"></div>
	        </div>
			
			@if ($errors->any())
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					<p>Non posso completare la richiesta:</p>
                    <ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
                    </ul>
                </div>
			@endif

	        <div class="x_content">	
	            <table id="table-categories" class="table table-striped">
	                <thead>
	                    <tr>
	                        <th></th>	                        
	                        <th>Nome</th>
	                        <th>Cognome</th>
	                        <th>Email</th>	                        
	                        <th>Azioni</th>
	                    </tr>
	                </thead>
	                <tbody>
	                
						@foreach($users as $user)
							<tr class="row-category">
								<td><div class="dot-color" style="background-color:{{ $user['color'] }};"></div></td>
								<td>{{ $user['name'] }}</td>
								<td>{{ $user['surname'] }}</td>
								<td>{{ $user['email'] }}</td>
								<td>
									<a href="{{ URL::action('UserController@edit', $user['id']) }}" class="action-link fa fa-pencil"></a>																									
									<a href="{{ URL::action('UserController@destroy', $user['id']) }}" onClick="return confirm('Sei sicuro di voler cancellare questa riga?')" class="action-link link-danger fa fa-close"></a>
								</td>
							</tr>
						@endforeach
						
	                </tbody>
	            </table>
	        </div>
	    </div>
	</div>	
</div>

@stop
