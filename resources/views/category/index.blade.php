@extends('app')

@section('content')

<div class="page-title">
	<div class="title_left"></div>
</div>

<div class="clearfix"></div>

<div class="row" id="app">
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	        <div class="x_title">
	        	<h2>@{{ title }}</h2>
	            <ul class="nav navbar-right panel_toolbox">
	                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
	            </ul>	        	
				<div class="clearfix"></div>
	        </div>
	        
			<div class="alert alert-danger alert-dismissible fade in" role="alert" v-if="errors.length > 0">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				<p>Non posso aggiungere la categoria:</p>
				<ul>
					<li v-for="error in errors">@{{ error }}</li>
				</ul>
			</div>

	        <div class="x_content">	
				<div class="well">
	                <form action="{{ URL::action("CategoryController@store") }}" method="POST" class="form-horizontal">
						<fieldset id="add-category">
	                        <div class="control-group">
	                            <div class="controls">
									<div class="input-prepend input-group">	                            
										<span class="add-on input-group-addon"><i class="glyphicon glyphicon-tag fa fa-tag"></i></span>
										<input type="text" style="width: 200px" name="name" id="new-category-name" class="form-control" v-model="new_category" value="">
										<input type="submit" id="new-category-button" class="btn btn-round btn-primary" v-on:click="addCategory" value="+" />
									</div>
	                            </div>
	                        </div>
	                    </fieldset>
	                </form>
	            </div>
			</div>
        </div>
	
	    <div class="x_panel">
	        <div class="x_title">
	            <h2>Categorie</h2>
	            <ul class="nav navbar-right panel_toolbox">
	                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
	            </ul>
	            
	            <div class="clearfix"></div>
	        </div>

	        <div class="x_content">	
	            <table id="table-categories" class="table table-striped">
	                <thead>
	                    <tr>
	                        <th>Nome</th>
	                        <th>Azioni</th>
	                    </tr>
	                </thead>
	                <tbody>
						<tr class="row-category" v-for="category in categories">
							<td>@{{ category.name }}</td>
							<td><a href="api/category" class="action-link link-danger del-link fa fa-close" v-on:click="delCategory(category.id, $event)"></a></td>
						</tr>
	                </tbody>
	            </table>
	        </div>
	    </div>
	</div>	
</div>

<script type="text/javascript">
	var app = new Vue({
		// Eleneto che identenfica l'App
		el: '#app',
		
		// Modello
		data: {
			title: 'Aggiungi categoria',
			new_category: "",
			categories: [],
			errors: []
		},
		
		// Metodo mounted, vedi: https://vuejs.org/v2/guide/instance.html
		mounted() {
			this.fetchCategories();
		}, 

		// Metodi
		methods: {
			/** 
			 * Ottengo le categorie 
			 */
			fetchCategories:function() {
				let url = '/api/category';
				var data = [];
				var headers = [];
			
				axios.get(url, data, headers).then(function(response) {
					app.categories = response.data.categories;

				}).catch(error => {
					console.log(error)
				});
			},

			/** 
			 * Aggiunge la categorie 
			 */
			addCategory:function(event) {
				event.preventDefault();
				
				let _token = $("meta[name='csrf-token']").attr('content');	// Ottengo il token con jQuery 				
				let url = '/api/category';
				var headers = [];
				var data = { 
					name: app.new_category, 
					_token: _token 
				};

				axios.post(url, data).then(response => {					
					if (response.data.errors) {
						app.errors = response.data.errors;
					
					} else {
						app.categories.push(response.data.category);
					}
				}).catch(error => {
					console.log(error);
      			});
			},

			/** 
			 * Rimuove la categoria 
			 */ 
			delCategory:function(id, event) {
				event.preventDefault();

				let url = '/api/category/' + id;
				var data = [];
				var headers = [];
			
				axios.delete(url, data, headers).then(function(response) {
					app.categories.forEach(function(item, index, object) {
  						if (item.id == id) {
    						object.splice(index, 1);
  						}
					});

				}).catch(error => {
					console.log(error)
				});
			}
		}
	});
</script>
	
@stop
