@extends('app')

@section('content')

<div class="page-title">
	<div class="title_left"></div>
</div>

<div class="clearfix"></div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	        <div class="x_title">
	            <h2>Aggiungi una spesa</h2>
	            <ul class="nav navbar-right panel_toolbox">
					<li><a class="" href="{{ URL::action('ExpenseController@index') }}"><i class="fa fa-close"></i></a></li>	                
	            </ul>
	            <div class="clearfix"></div>
	        </div>
	        <div class="x_content">
	            
	            <br>
	            
	            <form id="create-form" method="POST" action="{{ URL::action('ExpenseController@index') }}" class="form-horizontal form-label-left">
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

					@if ($errors->any())
						<div class="alert alert-create alert-danger alert-dismissible fade in" role="alert">
		                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<p>Non posso aggiungere la spesa perchè:</p>
		                    <ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
		                    </ul>
		                </div>
					@endif

					@if (session('success'))
						<div class="alert alert-create alert-success alert-dismissible fade in" role="alert">
		                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<p>{{ session('success') }}</p>
		                </div>
					@endif
					
	                <div class="form-group">
	                    <label for="date" class="control-label col-md-3 col-sm-3 col-xs-12">Data <span class="required">*</span>
	                    </label>
	                    <div class="col-md-6 col-sm-6 col-xs-12">
	                        <input type="text" id="payed_at" name="payed_at" value="{{ date('d/m/Y') }}" class="date-picker form-control col-md-7 col-xs-12">
	                    </div>
	                </div>
	                
	                <div class="form-group">
	                    <label for="category" class="control-label col-md-3 col-sm-3 col-xs-12">Categoria</label>	                    
	                    <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control" name="category_id">
								@foreach ($categories as $category)
	                                <option value="{{ $category->id }}">{{ $category->name }}</option>
								@endforeach
                            </select>
                        </div>
                    </div>
	                
	                <div class="form-group">
	                    <label for="description" class="control-label col-md-3 col-sm-3 col-xs-12">Descrizione<span class="required">*</span></label>
	                    <div class="col-md-6 col-sm-6 col-xs-12">
	                        <input type="text" id="description" name="description" class="form-control col-md-7 col-xs-12">
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label for="amount" class="control-label col-md-3 col-sm-3 col-xs-12">Spesa <span class="required">*</span></label>
	                    <div class="col-md-6 col-sm-6 col-xs-12">
	                        <input type="number" step="0.01" id="amount" name="amount" class="form-control col-md-7 col-xs-12">
	                    </div>
	                </div>
	                
	                <div class="form-group">
	                    <label for="amount" class="control-label col-md-3 col-sm-3 col-xs-12">Portafoglio <span class="required">*</span>
	                    </label>	                    
	                    <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control" name="user_id">
								@foreach ($users as $user)
	                                <option value="{{ $user->id }}">{{ $user->name }}</option>
								@endforeach
                            </select>
                        </div>
                    </div>
	                	                
	                <div class="ln_solid"></div>
	                <div class="form-group">
	                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                        <button type="submit" class="btn btn-primary">Aggiungi</button>
	                    </div>
	                </div>
	            </form>
	        </div>
	    </div>
	</div>
</div>

<!-- Custom script per Expense -->
<script type="text/javascript" src="{{ URL::asset('js/expense.js') }}"></script> 		

@stop
