@extends('app')

@section('content')

<div class="page-title">
	<div class="title_left"></div>
</div>

<div class="clearfix"></div>

<?php if (count($expenses) < 1): ?>

<div id="no-expenses">
	<h1>Non ci sono spese inserite</h1>
	<a href="{{ URL::action('ExpenseController@create') }}">Aggiungi una spesa</a>
</div>

<?php else: ?>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	        <div class="x_title">
	        	<h2>Imposta Periodo</h2>
	            <ul class="nav navbar-right panel_toolbox">
	                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
	            </ul>	        	
				<div class="clearfix"></div>
	        </div>
	        
	        <div class="x_content" style="display:none;">	
				<div class="well">
                    <fieldset id="period">
                        <div class="control-group">
                            <div class="controls">
								<form id="form-period" method="get" action="{{ URL::action('ExpenseController@index') }}" class="form-horizontal form-label-left">
	                                <div class="input-prepend input-group">
                                    	<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
										<input type="text" style="width: 200px" name="date-period-begin" id="date-period-begin" class="form-control date-period" value="{{ human_date($begin) }}">
										<input type="text" style="width: 200px" name="date-period-end" id="date-period-end" class="form-control date-period" value="{{ human_date($end) }}">
										<a href="javascript:void(0);" id="period-reset" style="margin-left: 5px; margin-right: 20px; font-size: 18px;"><i class="fa fa-refresh"></i></a>											
										<button type="submit" style="margin-left: 2px; margin-bottom: 0px;" id="submit-period" class="btn btn-primary">Imposta</button>
	                                </div>
								</form>
                            </div>
                        </div>
                    </fieldset>
	            </div>
			</div>
        </div>
	
	    <div class="x_panel">
	        <div class="x_title">
	            <h2>Spese dal {{ human_date($begin) }} al {{ human_date($end) }}</h2>
	            <ul class="nav navbar-right panel_toolbox">
	                <li><a class="add-link" href="{{ URL::action('ExpenseController@create') }}"><i class="fa fa-plus"></i></a></li>
	                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
	            </ul>
	            
	            <div class="clearfix"></div>
	        </div>

	        <div class="x_content">	
	            <table class="table table-striped">
	                <thead>
	                    <tr>
	                        <th>Data</th>
	                        <th>Categoria</th>
	                        <th>Note</th>
	                        <th>Spesa</th>
	                        <th>Portafoglio</th>	                        
	                        <th>Azioni</th>
	                    </tr>
	                </thead>
	                <tbody>
						@foreach($expenses as $e)
							<tr>
								<td>{{ date('d/m/Y', strtotime($e->payed_at)) }}</td>
								<td>{{ $e->categoryName() }}</td>
								<td>{{ $e->description }}</td>
								<td>{{ number_format($e->amount, 2) }}€</td>																
								<td>{{ $e->wallet() }}</td>																
								<td>
									<a href="{{ URL::action('ExpenseController@edit', $e->id) }}" class="action-link fa fa-pencil"></a>																
									<a href="{{ URL::action('ExpenseController@destroy', $e->id) }}" class="action-link link-danger del-link fa fa-close"></a>
								</td>
							</tr>
						@endforeach
	                </tbody>
	            </table>
	        </div>
	    </div>
	</div>	
</div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	        <div class="x_title">
	            <h2>Riassunto</h2>
	            <ul class="nav navbar-right panel_toolbox">
	                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
	            </ul>
	            <div class="clearfix"></div>
	        </div>

	        <div class="x_content">	        
	        	<div class="col-md-6 col-sm-6 col-xs-12">
		            <table id="table-summary" class="table table-striped">
		                <thead>
		                    <tr>
		                        <th></th>
		                        <th>Portafoglio</th>
		                        <th>Spesa</th>
		                    </tr>
		                </thead>

		                <tbody>			
		                	@foreach ($user_expenses as $expense)
								<tr>
									<td><div class="dot-color" style="background-color:{{ $expense['color'] }}"></div></td>											
									<td>{{ $expense['name'] }}</td>
									<td>{{ number_format($expense['amount'], 2) }}€</td>			
								</tr>
		                	@endforeach
		                </tbody>

						<tfoot>
							<tr>
		                        <th></th>							
								<th><strong>Totale</strong></th>
								<th>{{ number_format($total_expenses, 2) }}€</th>																
							</tr>
						</tfoot>		                
		            </table>	
		            
					<br/>		            
		            
					<table id="table-summary" class="table table-striped">
		                <thead>
		                    <tr>
		                        <th></th>
								@foreach ($user_expenses as $expense)
									<th>{{ $expense['name'] }}</th>
								@endforeach
		                    </tr>
		                </thead>

		                <tbody>			
							@foreach ($user_expenses as $expense)
								<tr>
									<td>{{ $expense['name'] }}</td>									
									@foreach ( $expense['to_give'] as $to_give)
										<td>{{ number_format($to_give, 2) }}€</td>	
									@endforeach
								</tr>								
							@endforeach
						</tbody>
		            </table>		            
	        	</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="hidden" id="encoded_user_list" value="{{ json_encode($users) }}" />
					<input type="hidden" id="encoded_user_expense_list" value="{{ json_encode($user_expenses) }}" />
					<canvas id="canvas_pie"></canvas>	        
				</div>	        
	        </div>
	    </div>
	</div>	
</div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	        <div class="x_title">
	            <h2>Ripartizione spese</h2>
	            <ul class="nav navbar-right panel_toolbox">
	                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
	            </ul>
	            <div class="clearfix"></div>
	        </div>

	        <div class="x_content">	        
	        	<div class="col-md-6 col-sm-6 col-xs-12">
		            <table id="table-split" class="table table-striped">
		                <thead>
		                    <tr>
		                        <th></th>		                    
		                        <th>Categoria</th>
		                        <th>Spesa</th>
		                    </tr>
		                </thead>

		                <tbody>		
		                	@foreach ($category_expenses as $index => $expense)
								<tr class="row-category-id-{{ $expense->category_id }}">
									<td><div class="dot-color color color-{{ $index }}"></div></td>											
									<td>{{ $expense->name }}</td>
									<td>{{ number_format($expense->total_amount, 2) }}€</td>
								</tr>
		                	@endforeach
		                </tbody>
		            </table>	        	
	        	</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="hidden" id="encoded_category_expense_list" value="{{ json_encode($category_expenses) }}" />
					<canvas id="split_pie"></canvas>	        
				</div>	        
	        </div>
	    </div>
	</div>	
</div>

<script type="text/javascript">	
	/** 
	 * Gestione del click sulla X della riga 
	 */
	 $(document).on('click', 'a.del-link', function(e) {
		e.preventDefault();
		
		if (!confirm('Sei sicuro di voler cancellare questa riga?')) {
			return;
		}

		// Ottengo  la riga della tabella cercando fa i parents del bottone l'elemento <tr>
		var row = $(this).parents('tr');	
		// L'URL lo prendo direttamente dal link 
		var url = $(this).attr('href');	
		// Ottengo il token del form perchè mi serve anche per l'azione che sto per compiere 
		var _token = $("meta[name='csrf-token']").attr('content');	
		
		$.ajax({
			url: url,
			type: "DELETE",	// Uso appunto il metodo DELETE
			dataType: "json",  
			data: { '_token': _token }, // Passo l'id della categria e il token 
			success: function(data) {                        
				if (data.status === 'ok') {
					
					// Qui ho usato un semplice remove() ma potrei usare un fadeOut() o altro 
					$(row).remove();	
					
					/** 
					 * ATTENZIONE!!! I più attenti si renderanno conto che c'è qualcosa che non và 
					 * come si può migliorare la situazione?
					 * 
					 */
				}
			}, 
			error: function(response, stato) {
				console.log(stato);
			}
		});
	});

	/** 
	 * In questo script gestisco la generazione dei grafici.
	 */ 
	$(document).ready(function() { 	    
		
		/* Spese per utente */ 

		var users = JSON.parse( $('#encoded_user_list').val() );
		var user_expenses = JSON.parse( $('#encoded_user_expense_list').val() );

		var colours = [];
		for (i=0; i < users.length; i++) {
			colours.push(users[i].color);	
		}

		var summaryData = [];
		for (i=0; i < user_expenses.length; i++) {
			var data = {
				value: parseFloat(user_expenses[i].amount),
				color: colours[user_expenses[i].user_id -1],
				highlight: colours[user_expenses[i].user_id -1],
				label: user_expenses[i].name,
				user_id: user_expenses[i].user_id,
			}

			summaryData.push(data);	
		}

		window.myPie = new Chart(document.getElementById("canvas_pie").getContext("2d")).Pie(summaryData, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

		/* Categorie delle spese */ 

		var category_expense = JSON.parse( $('#encoded_category_expense_list').val() );

		var splitData 	= [];
		for (i=0; i < category_expense.length; i++) {
			var _color = randomColor(i);
			var data = {
				value: parseFloat(category_expense[i].total_amount),
				color: _color,
				highlight: _color,
				label: category_expense[i].name,
				category_id: category_expense[i].category_id,
			}
			
			splitData.push(data);	
			
			$('#table-split .color-' + i).css('background-color', _color);
		}

		window.myPie = new Chart(document.getElementById("split_pie").getContext("2d")).Pie(splitData, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

		/** 
		 * Ritorna il colore all'indice passato come argomento.
		 * Se viene passato un indice negativo o che supera il numero di colori disponibili allora viene generato un indice casualmente.
		 */ 
		function randomColor(index) {
			var colours = ['#4E9FA5','#F26522', '#FFCD33', '#4E7EA5', '#A54E7C', '#676766'];
			if (index < 0 || index > colours.length-1) {
				return getRandomColor();
			}
			
			return colours[index]
		}

		/** 
		 * Ritorna un colore generto casualmente 
		 */ 
		function getRandomColor() {
			var letters = '0123456789ABCDEF'.split('');
			var color = '#';
			for (var i = 0; i < 6; i++ ) {
				color += letters[Math.floor(Math.random() * 16)];
			}	
			
			return color;
		}
	});
</script>

<!-- Custom script per Expense -->
<script type="text/javascript" src="{{ URL::asset('js/expense.js') }}"></script> 		

<?php endif; // if count $expenses > 1 ?>

@stop
